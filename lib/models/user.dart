part of 'models.dart';

class User extends Equatable {
  final int id;
  final String name;
  final String email;
  final String address;
  final String houseNumber;
  final String phoneNumber;
  final String city;
  final String picturePath;

  User(
      {this.id,
      this.name,
      this.email,
      this.address,
      this.houseNumber,
      this.phoneNumber,
      this.city,
      this.picturePath});
  @override
  
  List<Object> get props =>
      [id, name, email, address, houseNumber, phoneNumber, city, picturePath];
}

User mockUser = User(
    id: 1,
    name: 'Ulul Arkham',
    address: 'Wanusobo',
    city: 'Jepara',
    houseNumber: '1234',
    phoneNumber: '08945467454',
    email: '171240000682@unisnu.ac.id',
    picturePath:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSYwcdevRPUkCZIOr9jjfkayW-YdsDKMIw67w&usqp=CAU');
